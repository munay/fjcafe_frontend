import React, { useState, useEffect } from 'react';

const EntregarPedido = () => {
  const [pedidos, setPedidos] = useState([]);
  const [productos, setProductos] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8000/pedidos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setPedidos(data);
      });
  }, []);

  useEffect(() => {
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProductos(data);
      });
  }, []);

  const pedidosListos = pedidos.filter((pedido) => pedido.estado === 'Listo');
  const pedidosEntregados = pedidos.filter((pedido) => pedido.estado === 'Entregado');

  const handleUpdatePedido = (pedidoId, cli, nummesa, lista_pro) => {
    const updatedPedidos = pedidos.map((pedido) => {
      if (pedido.id === pedidoId) {
        return { ...pedido, estado: 'Entregado' };
      }
      return pedido;
    });
    fetch(`http://localhost:8000/pedidos/${pedidoId}/`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        estado: 'Entregado',
        cliente: cli,
        mesa: nummesa,
        lista_productos: lista_pro,
      }),
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Error al actualizar el pedido');
        }
      })
      .then((data) => {
        setPedidos(updatedPedidos);
      })
      .catch((error) => {
        console.error('Error al actualizar el pedido:', error);
      });
  };

  return (
    <>
      <h2 className="h3">Pedidos Listos</h2>
      <div className="lista-pedidos"> 
        {pedidosListos.length === 0 ? (
          <p>No hay pedidos listos</p>
        ) : (
          <div className="lista-pedidos" >
            {pedidosListos.map((pedido) => (
              <div className="tarjeta-pedido" key={pedido.id}>
                <h4 className="t4">Pedido #{pedido.id}</h4>
                <h4  className="t5">Cliente: {pedido.cliente}</h4 >
                <h4 className="t5">Mesa: {pedido.mesa}</h4>
                <h4 className='letra-cocinero'>Productos</h4>
                  {JSON.parse(pedido.lista_productos).map((productoId) => {
                    const producto = productos.find((p) => p.id === productoId);
                    return (
                      <div className="letra-prod" key={producto.id}>
                        <h4 className="letra-prod">{producto.nombre}  {producto.precio}</h4>
                      </div>
                    );
                  })}
                {pedido.estado === 'Listo' && (
                  <button className="enviar"
                    onClick={() =>
                      handleUpdatePedido(
                        pedido.id,
                        pedido.cliente,
                        pedido.mesa,
                        JSON.parse(pedido.lista_productos)
                      )
                    }
                  >
                    Entregado
                  </button>
                )}
                  
               
              </div>
            ))}
          </div>
        )} 
      </div> 
      <h2 className="h3">Pedidos Entregados</h2>               
      <div className="lista-cocinero">
        {pedidosEntregados.length === 0 ? (
          <p>No hay pedidos entregados</p>
        ) : (
          pedidosEntregados.map((pedido) => (
            <div className="tarjeta-pedido" key={pedido.id}>
              <h3 className="letra-cocinero">Pedido: {pedido.id}</h3>
              <h3 className="letra-cocinero">Mesa: {pedido.mesa}</h3>
              <h3 className="letra-cocinero">Cliente: {pedido.cliente}</h3>
            </div>
          ))
        )}
      </div>
    </>
  );
};

export default EntregarPedido;