import React, { Suspense } from 'react';
import './app.css';
import { Nav, Navbar } from 'react-bootstrap';

const Menu = ({ role, setOpcionSeleccionada }) => {
  const handleMenuClick = (opcion) => {
    setOpcionSeleccionada(opcion);
  };

  return (
   
      <div className="menu">
        <Navbar collapseOnSelect expand="lg" variant="primary" outline>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav>
              {role === 'recepcion' && (
                <>
                  <Nav.Link onClick={() => handleMenuClick('pedidos')}>Visualizar Pedidos</Nav.Link>
                  <Nav.Link onClick={() => handleMenuClick('tomarpedidos')}>Tomar Pedidos</Nav.Link>
                  
                </>
              )}
              {role === 'cocina' && (
                <Nav.Link onClick={() => handleMenuClick('verpedidos')}>Ver Pedidos</Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
   
  );
};

export default Menu;
