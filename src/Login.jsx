import { useState } from 'react'
import coffeeIcon from './assets/coffee.svg'
import jwtDecode from 'jwt-decode'

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const loginHandle = (e) => {
    e.preventDefault()
    // login and get an user with JWT token
    fetch('http://localhost:8000/api/token/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((res) => res.json())
      .then((tokenData) => {
        window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
        console.log(tokenData);
        /*
        |Se agregó la librería jwt-decode (npm install jwt-decode) https://www.npmjs.com/package/jwt-decode 
        para decodificar el user_id del token recibido y poder acceder a los recursos de la aplicación que 
        necesiten autorización. 
        */
        console.log(jwtDecode(tokenData.access).user_id);
        onLogin(jwtDecode(tokenData.access).user_id)
      })
  }

  return (
    <form onSubmit={loginHandle}>
      <img src={coffeeIcon} alt="Coffee Icon" width={100} />
      <h2>Hora del Café!</h2>
      <p>¿Cómo te gusta el café? A nosotros, contigo</p>
          
      <input
        aria-label="Username"
        placeholder="Username"
        id="username"
        type="text"
        onChange={(e) => {
          setUsername(e.target.value)
        }}
      />
      <input
        aria-label="Password"
        placeholder="Password"
        id="password"
        type="password"
        onChange={(e) => {
          setPassword(e.target.value)
        }}
      />
      <button type="submit">Iniciar sesion</button>      
    </form>
  )
}

export default Login
