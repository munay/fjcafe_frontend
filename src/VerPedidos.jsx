import React, { useState, useEffect } from 'react';
import { Card, Button, Row, Col } from 'react-bootstrap';


const VerPedidos = () => {
  const [pedidos, setPedidos] = useState([]);
  const [productos, setProductos] = useState({});

  const formatoHora = (timeString) => {
    const time = new Date(timeString);
    const minutes = time.getMinutes().toString().padStart(2, '0');
    const seconds = time.getSeconds().toString().padStart(2, '0');
    return `${minutes}:${seconds}`;
  };

  useEffect(() => {
    fetch('http://localhost:8000/pedidos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setPedidos(data);
      });
  }, []);



useEffect(() => {
  const fetchProductos = async () => {
    const fetchPromises = pedidos.map((pedido) => {
      return fetch(`http://127.0.0.1:8000/pedidos/${pedido.id}/productos/`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${JSON.parse(
            window.localStorage.getItem('accessToken')
          )}`,
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => ({
          pedidoId: pedido.id,
          productos: data,
        }))
        .catch((error) => {
          console.error('Error:', error);
          return {
            pedidoId: pedido.id,
            productos: [],
          };
        });
    });

    const results = await Promise.all(fetchPromises);
    const updatedProductos = results.reduce((acc, { pedidoId, productos }) => {
      return {
        ...acc,
        [pedidoId]: productos,
      };
    }, {});

    setProductos(updatedProductos);
  };

  fetchProductos();
}, [pedidos]);

  
const pedidosListos = pedidos.filter((pedido) => pedido.estado === 'listo');
const pedidosPendientes = pedidos.filter((pedido) => pedido.estado === 'pendiente');

  const actualizarEstadoPedido = (pedidoId, vcliente, vnromesa, vlista_productos) => {
    const updatedPedidos = pedidos.map((pedido) => {
      if (pedido.id === pedidoId) {
        return { ...pedido, estado: 'listo' };
      }
      return pedido;
    });
    fetch(`http://localhost:8000/pedidos/${pedidoId}/`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        estado: 'listo',
        cliente: vcliente,
        mesa: vnromesa,
        lista_productos: vlista_productos,
        fecha_fin : new Date(),
      }),
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Error al actualizar el pedido');
        }
      })
      .then((data) => {
        setPedidos(updatedPedidos);
      })
      .catch((error) => {
        console.error('Error al actualizar el pedido:', error);
      });
  };

  const calcularTiempoTranscurrido = (fechaInicio, fechaFin) => {
    const inicio = new Date(fechaInicio);
    const fin = new Date(fechaFin);
    const diff = Math.abs(fin.getTime() - inicio.getTime());
    const minutes = Math.floor((diff / (1000 * 60)) % 60);
    const seconds = Math.floor((diff / 1000) % 60);
    return `${minutes}m ${seconds}s`;
  };

  return (
    <>
      <div className="d-flex justify-content-between">
        <div>
          <h2 className="tit-pendientes">Pedidos Pendientes</h2>
          <div className="container-pendientes">
            {pedidosPendientes.length === 0 ? (
              <p>No hay pedidos pendientes</p>
            ) : (
              <Row>
                {pedidosPendientes.map((pedido) => (
                  <Col key={pedido.id} sm={10} style={{ marginBottom: '1rem' }}>
                    <Card>
                      <Card.Body>
                        <Card.Title style={{ fontSize: '1rem' }}>Mesa: {pedido.mesa}</Card.Title>
                        <Card.Title style={{ fontSize: '1rem' }}>{pedido.cliente}</Card.Title>
                        <Card.Text>
                          {Array.isArray(productos[pedido.id]) ? (
                            productos[pedido.id].map((producto) => (
                              <div key={producto.id}>
                                <span className="alineado-izq-span">
                                  * {producto.nombre}.
                                </span>
                              </div>
                            ))
                          ) : null}
                        </Card.Text>
                        <Button
                          variant="primary"
                          onClick={() =>
                            actualizarEstadoPedido(
                              pedido.id,
                              pedido.cliente,
                              pedido.mesa,
                              JSON.parse(pedido.lista_productos),
                            )
                          }
                        >
                          Listo
                        </Button>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </div>
        </div>
        <div>
          <h2 className="tit-listos">Pedidos Listos</h2>
          <div className="container-listos">
            {pedidosListos.length === 0 ? (
              <p>**No hay***</p>
            ) : (
              <Row>
                {pedidosListos.map((pedido) => (
                  <Col key={pedido.id} sm={10} style={{ marginBottom: '1rem' }}>
                    <Card>
                      <Card.Body>
                        <Card.Title style={{ fontSize: '1rem' }}>Mesa: {pedido.mesa}</Card.Title>
                        <Card.Title style={{ fontSize: '1rem' }}>{pedido.cliente}</Card.Title>
                        <Card.Text>
                          {Array.isArray(productos[pedido.id]) ? (
                            productos[pedido.id].map((producto) => (
                              <div key={producto.id}>
                                <span className="alineado-izq-span">
                                  * {producto.nombre}.
                                </span>
                              </div>
                            ))
                          ) : null}
                        </Card.Text>
                        <Card.Text>
                          Tiempo transcurrido: {calcularTiempoTranscurrido(pedido.fecha_inicio, pedido.fecha_fin)}
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default VerPedidos;
