import React, { useState, useEffect } from 'react';
import Productos from './Productos';
import Pedidos from './Pedidos';
import TomarPedidos from './TomarPedidos';
import VerPedidos from './VerPedidos';
import Menu from './menu'; 
import './App';

const Home = ({ onLogout, userId }) => {
  const [user, setUser] = useState(null);
  //new
  const [opcionSeleccionada, setOpcionSeleccionada] = useState(null);
  //new

  useEffect(() => {
    fetch(`http://localhost:8000/users/${userId}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((userData) => {
        setUser(userData);
      });
  }, []);

  const logoutHandler = () => {
    onLogout();
  };

  const role = user ? user.group_name : null;

  const content = () => {
    if (role && role === 'recepcion') {
      if (opcionSeleccionada === 'productos') {
        return <Productos />;   
      } else if (opcionSeleccionada === 'pedidos') {
        return <Pedidos />;
      } else if (opcionSeleccionada === 'tomarpedidos') {
        return <TomarPedidos/>;
      }

    } 
    else if (role && role === 'cocina') {
      if (opcionSeleccionada === 'verpedidos') {
        return <VerPedidos />;
      }
    }
   // return <p>Página Principal</p>;
  };

  return (
    <div className="container">
      <header className="header">
        <h1>Bienvenido {user && user.username}!</h1>
      </header>

      <div className="main-content">
        <Menu role={role} setOpcionSeleccionada={setOpcionSeleccionada} />
        <div className="content-box">
          {content()}
        </div>         
      </div>
      <footer className="footer">
        <button className="button" onClick={logoutHandler}>Logout</button>
      </footer>
    </div>
  );
};

export default Home;
