import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Card, Button, Row, Col } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
//import { useNotificationCenter } from 'react-toastify/addons/use-notification-center';

const TomarPedidos = () => {
  const [productos, setProductos] = useState([]);
  const [seleccionados, setSeleccionados] = useState([]);
  const [totalPrecio, setTotalPrecio] = useState(0);
  const [nromesa, setNroMesa] = useState('');
  const [cliente, setCliente] = useState('');
  const [pedidoEnviado, setPedidoEnviado] = useState(false);

  useEffect(() => {
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProductos(data);
      });
  }, []);

  const agregarSeleccionado = (producto) => {
    setSeleccionados([...seleccionados, producto]);
    setTotalPrecio(totalPrecio + producto.precio);
  };

  const eliminarSeleccionado = (producto) => {
    const index = seleccionados.indexOf(producto);
    if (index > -1) {
      const nuevosSeleccionados = [...seleccionados];
      nuevosSeleccionados.splice(index, 1);
      setSeleccionados(nuevosSeleccionados);
      setTotalPrecio(totalPrecio - producto.precio);
    }
  };

  const guardarPedido = () => {
    console.log(cliente.trim());
    if (cliente.trim() === '') {
      toast.error('Por favor, ingrese el nombre del cliente.', {
        position: toast.POSITION.TOP_CENTER,
      });
      return;
    }

    if (isNaN(nromesa) || nromesa <= 0) {
      toast.error('Por favor, ingrese un número de mesa válido.');
      return;
    }

    const lista_pro = seleccionados.map((producto) => producto.id);
    fetch('http://localhost:8000/pedidos/', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        mesa: nromesa,
        cliente: cliente,
        lista_productos: lista_pro,
      }),
    })
      .then((res) => {
        if (res.status === 201) {
          setPedidoEnviado(true);
          toast.success('Pedido enviado');
          setNroMesa('');
          setCliente('');
          setSeleccionados([]);
          setTotalPrecio(0);
        } else {
          setPedidoEnviado(true);
          toast.error('No se pudo enviar el pedido. Por favor, complete todos los datos.');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
      });
  };

  return (
    <>
      <div className="container">
        <div className="columna-wrap" style={{ display: 'inline-block', marginRight: '10px' }}>
          <h2 className="h4">Pedido</h2>
          <div className="container">
            <label htmlFor="nromesa" className="h4">
              Nro. mesa:
            </label>
            <input
              id="nromesa"
              type="text"
              className="mesa"
              style={{ display: 'inline-block' }}
              value={nromesa}
              onChange={(e) => setNroMesa(e.target.value)}
            />
          </div>
          <div className="container">
            <label htmlFor="cliente" className="h4">
              Cliente:
            </label>
            <input
              id="cliente"
              type="text"
              className="input-cliente"
              placeholder="Nombre"
              value={cliente}
              onChange={(e) => setCliente(e.target.value)}
            />
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="container">
              <h3 className="h3">Productos</h3>
              <Table striped>
                <tbody>
                  <div>
                  {productos.map((producto) => (
                    <tr key={producto.id}>
                      <td>
                        <h5 className="h6">{producto.nombre}</h5>
                        <h5 className="h6">Precio: {producto.precio}</h5>
                      </td>
                      <td>
                        <button
                          variant="primary"
                          onClick={() => agregarSeleccionado(producto)}                     
                        >
                          Agregar
                        </button>
                       </td>
                    </tr>
                    ))}
                  </div>
                </tbody>
              </Table>             
            </div>
          </div>
          <div className="col-md-6"> 
            <div className="container">
              <h3 className="h3">Seleccionados</h3>
              <Table striped className="h6">
             <tbody>
              <div className="container">              
                  {seleccionados.map((producto) => {
                    return (
                      <tr key={producto.id}>
                        <td className="h6">
                          {producto.nombre} - {producto.precio}
                        </td>
                        <td>
                        <button
                          variant="primary"                          
                          onClick={() => eliminarSeleccionado(producto)}
                        >
                          Eliminar
                        </button>   
                       </td>                   
                    </tr>
                    );
                  })}                
              </div>
              </tbody>
              </Table>
            </div>
          </div>
        </div>
        <div className="container">
          <h3 className="container">Total Precio: {totalPrecio}</h3>
          <button 
            variant="primary"
            onClick={guardarPedido} className="enviar">
            Enviar Pedido
          </button>
        </div>
      </div>
      {pedidoEnviado && <ToastContainer />}
      <ToastContainer />
    </>
  );
};

export default TomarPedidos;
