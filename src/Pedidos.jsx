import React, { useState, useEffect } from 'react';
import { Card, Button, Row, Col } from 'react-bootstrap';

const Pedidos = () => {
  const [pedidos, setPedidos] = useState([]);
  const [productos, setProductos] = useState({});

  useEffect(() => {
    fetch('http://localhost:8000/pedidos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setPedidos(data);
      });
  }, []);

  useEffect(() => {
    pedidos.forEach((pedido) => {
      fetch(`http://127.0.0.1:8000/pedidos/${pedido.id}/productos/`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${JSON.parse(
            window.localStorage.getItem('accessToken')
          )}`,
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          setProductos((prevProductos) => ({
            ...prevProductos,
            [pedido.id]: data,
          }));
        })
        .catch((error) => console.error('Error:', error));
    });
  }, [pedidos]);

  const pedidosListos = pedidos.filter((pedido) => pedido.estado === 'listo');
  // const pedidosEntregados = pedidos.filter((pedido) => pedido.estado === 'Entregado');
  const pedidosEntregados = pedidos.filter((pedido) => {
    if (pedido.estado === 'Entregado') {
      const unaHoraAtras = new Date();
      unaHoraAtras.setHours(unaHoraAtras.getHours() - 1);
  
      const fechaInicio = new Date(pedido.fecha_inicio);
      return fechaInicio.getTime() >= unaHoraAtras.getTime();
    }
    return false;
  });
  const pedidosPendientes = pedidos.filter((pedido) => pedido.estado === 'pendiente');
  const actualizarEstadoPedido = (pedidoId, vcliente, vnromesa, vlista_productos) => {
    const updatedPedidos = pedidos.map((pedido) => {
      if (pedido.id === pedidoId) {
        return { ...pedido, estado: 'Entregado' };
      }
      return pedido;
    });
    fetch(`http://localhost:8000/pedidos/${pedidoId}/`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        estado: 'Entregado',
        cliente: vcliente,
        mesa: vnromesa,
        lista_productos: vlista_productos,
      }),
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Error al actualizar el pedido');
        }
      })
      .then((data) => {
        setPedidos(updatedPedidos);
      })
      .catch((error) => {
        console.error('Error al actualizar el pedido:', error);
      });
  };

  return (
    <>
      <div className="d-flex justify-content-between">
      <Card>
          <h2 className="tit-pendientes">Pedidos Pendientes</h2>
          <div className="container-pendientes">
            {pedidosPendientes.length === 0 ? (
              <p>**Sin pedidos pendientes**</p>
            ) : (
              <Row>
                {pedidosPendientes.map((pedido) => (
                  <Col key={pedido.id}>
                    <Card>
                      <Card.Body>
                        <Card.Title style={{ fontSize: '1rem' }}>Mesa: {pedido.mesa}</Card.Title>
                        <Card.Title style={{ fontSize: '1rem' }}>{pedido.cliente}</Card.Title>
                        <Card.Text>
                          {Array.isArray(productos[pedido.id]) ? (
                            productos[pedido.id].map((producto) => (
                              <div key={producto.id}>
                                <span className="alineado-izq-span">
                                  {producto.nombre}. Gs.{producto.precio}
                                </span>
                              </div>
                            ))
                          ) : null}
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </div>
        </Card>
        <Card>
          <h2 className="tit-listos">Pedidos Listos</h2>
          <div className="container-listos">
            {pedidosListos.length === 0 ? (
              <p>**Sin Pedidos a listos para entrega**</p>
            ) : (
              <Row>
                {pedidosListos.map((pedido) => (
                  <Col key={pedido.id} >
                    <Card>
                      <Card.Body>
                        <Card.Title style={{ fontSize: '1rem' }}>Mesa: {pedido.mesa}</Card.Title>
                        <Card.Title style={{ fontSize: '1rem' }}>{pedido.cliente}</Card.Title>
                        <Card.Text>
                          {Array.isArray(productos[pedido.id]) ? (
                            productos[pedido.id].map((producto) => (
                              <div key={producto.id}>
                                <span className="alineado-izq-span">
                                  * {producto.nombre}.
                                </span>
                              </div>
                            ))
                          ) : null}
                        </Card.Text>
                        <Button
                          variant="primary"
                          onClick={() =>
                            actualizarEstadoPedido(
                              pedido.id,
                              pedido.cliente,
                              pedido.mesa,
                              JSON.parse(pedido.lista_productos)
                            )
                          }
                        >
                          Entregado
                        </Button>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </div>
        </Card>
        <Card>
          <h2 className="tit-entregados">Pedidos Entregados</h2>
          <div className="container-entregados">
            {pedidosEntregados.length === 0 ? (
              <p>**Sin Pedidos entregados**</p>
            ) : (
              <Row>
                {pedidosEntregados.map((pedido) => (
                  <Col key={pedido.id}>
                    <Card>
                      <Card.Body>
                        <Card.Title style={{ fontSize: '1rem' }}>Mesa: {pedido.mesa}</Card.Title>
                        <Card.Title style={{ fontSize: '1rem' }}>{pedido.cliente}</Card.Title>
                        <Card.Text>
                          {Array.isArray(productos[pedido.id]) ? (
                            productos[pedido.id].map((producto) => (
                              <div key={producto.id}>
                                <span className="alineado-izq-span">
                                  * {producto.nombre}.
                                </span>
                              </div>
                            ))
                          ) : null}
                        </Card.Text>                       
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </div>
        </Card>
      </div>
    </>
  );
};

export default Pedidos;
