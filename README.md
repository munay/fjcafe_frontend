Para ejecutar el proyecto de la cafetería sigue estos pasos:
- Instalar Node.js. Sitio oficial de descarga: https://nodejs.org
- Descargar el código fuente del proyecto de la cafetería en tu computadora. Esto lo puedes hacer clonando los proyectos -> Backend con git clone https://gitlab.com/munay/fjcafe_backend.git. Para el Frontend -> https://gitlab.com/munay/fjcafe_frontend 
- Abrir una terminal y navega hasta el directorio raíz del proyecto de la cafetería.
- Crear un entorno virtual con python -m venv venv y luego activarlo con env\Scripts\activate.bat
- Para instalar las dependencias necesarias primero instalar y actualizar pip con python -m pip install --upgrade pip
- Instalar las dependencias necesarias con pip install -r requirements.txt
- Crear las entidades necesarias en la base de datos mongodb con python .\manage.py makemigrations y
python .\manage.py migrate
- Crear usuario administrador de django con python .\manage.py createsuperuser
- Iniciar el servidor backend con python .\manage.py runserver
- Abrir el navegador y acceder a la siguiente URL http://127.0.0.1:8000/ y cargar los datos necesarios grupos (rol), usuarios y productos
- Instalar npm install 
- Una vez completada la instalación de las dependencias, ejecuta el siguiente comando para iniciar el servidor de desarrollo: npm run dev (Esto iniciará el servidor de desarrollo y se compilará el proyecto)
- Abre tu navegador web y visita la URL proporcionada en la terminal (http://localhost:5173/). Esto cargará la aplicación de la cafetería en tu navegador.
- Ya puedes utilizar la aplicación de la cafetería en tu entorno de desarrollo local! 
